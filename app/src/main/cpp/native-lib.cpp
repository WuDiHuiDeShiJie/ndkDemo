#include <jni.h>
#include <string>
#include "Ding.h"

extern "C" JNIEXPORT jstring JNICALL
Java_com_ding_myapplication_MainActivity_stringFromJNI(
        JNIEnv *env,
        jobject /* this */) {
//
    Ding *d = new Ding();
//    std::string hello =  d->showChar() ;

    char chTemp[1024] = {0};
    std::string strTemp = "test";

    int iTemp = d->showValue();

    sprintf(chTemp, "%d%s", iTemp, d->showChar());

    std::string hello = chTemp;

    return env->NewStringUTF(hello.c_str());
}
