#include <jni.h>


#ifndef _Included_android_serialport_api_SerialPort
#define _Included_android_serialport_api_SerialPort
#ifdef __cplusplus
extern "C" {
#endif


JNIEXPORT jstring JNICALL
Java_com_ding_myapplication_MainActivity_open(JNIEnv *env, jobject thiz);

#ifdef __cplusplus
}
#endif
#endif